import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}


class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Shared Preference",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late SharedPreferences prefs;
  TextEditingController email = new TextEditingController();
  TextEditingController password_ = new TextEditingController();

  late SharedPreferences _preferences;
  String username = "";

  void initState() {
    super.initState();
    admin();
  }

  void admin() async {
    prefs = await SharedPreferences.getInstance();
    username = prefs.getString("Email") ?? '';
    setState(() {
      email.text = username;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('LoginPage'),
      ),
      body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [Image.asset('assets/flutterlogo.png',width: 300,height: 200, ),
              TextField(
                controller: email,
                decoration: InputDecoration(
                  hintText: 'Email',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                controller: password_,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Password',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 40,
                width: 200,
                child: ElevatedButton(
                  child: Text('Login',style: TextStyle(fontSize: 20)),
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                            )
                        ),
                    ),
                  onPressed: () {
                   login();
                  },

                ),
              ),
            ],
          )),
    );
  }

  login() async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString("Email", email.text.toString());

  }
}
